## Dcoker compose
```compose.yml
version: "3"
services:
  web:
    image: edtasixm05/getstarted:comptador 
    deploy:
      replicas: 2
      restart_policy:
        condition: on-failure
      resources:
        limits:
          cpus: "0.1"
          memory: 50M
    ports:
      - "80:80"
    networks:
      - webnet
  redis:
    image: redis
    ports:
      - "6379:6379"
    deploy:
      placement:
        constraints: [node.role == manager]
    command: redis-server --appendonly yes
    networks:
      - webnet
  visualizer:
    image: dockersamples/visualizer:stable
    ports:
      - "8080:8080"
    volumes:
      - "/var/run/docker.sock:/var/run/docker.sock"
    deploy:
      placement:
        constraints: [node.role == manager]
    networks:
      - webnet
  portainer:
    image: portainer/portainer
    ports:
            - "9000:9000"
    volumes:
      - "/var/run/docker.sock:/var/run/docker.sock"
    deploy:
      placement:
        constraints: [node.role == manager]
    networks:
      - webnet
networks:
  webnet:

```

## Engegar swarm
```
a221351ca@i17:~/m14-reptes/reptes_swarm/repte2$ docker swarm init 
Swarm initialized: current node (wss1nnbjza67mluuqp8touanb) is now a manager.

To add a worker to this swarm, run the following command:

    docker swarm join --token SWMTKN-1-0243ab8vxwn9pzfov7ywtup662ci6f7s0es3g7ud6v1jf152q8-98wydb3j74v1mzx6fxdd13le1 10.200.243.217:2377

To add a manager to this swarm, run 'docker swarm join-token manager' and follow the instructions.

```

```
a221351ca@i17:~/m14-reptes/reptes_swarm/repte2$ docker stack deploy -c compose.yml myapp
Updating service myapp_portainer (id: 27ra79h13hp7jm0j3ytso8aoj)
Creating service myapp_web
Creating service myapp_redis
Creating service myapp_visualizer
```

## Scale
```
a221351ca@i17:~/m14-reptes/reptes_swarm/repte2$ docker service ls
ID             NAME               MODE         REPLICAS   IMAGE                                 PORTS
olhcqvj91sdp   myapp_redis        replicated   1/1        redis:latest                          *:6379->6379/tcp
qlhij5qz0i68   myapp_visualizer   replicated   1/1        dockersamples/visualizer:stable       *:8080->8080/tcp
mioomfkfo6ce   myapp_web          replicated   2/2        cesaraguilarmendez/comptador:latest   *:80->80/tcp
```
```
a221351ca@i17:~/m14-reptes/reptes_swarm/repte2$ docker service scale myapp_web=2
myapp_web scaled to 2
overall progress: 2 out of 2 tasks 
1/2: running   
2/2: running   
verify: Service converged 
```

```
a221351ca@i17:~/m14-reptes/reptes_swarm/repte2$ docker service ls
ID             NAME               MODE         REPLICAS   IMAGE                                 PORTS
olhcqvj91sdp   myapp_redis        replicated   1/1        redis:latest                          *:6379->6379/tcp
qlhij5qz0i68   myapp_visualizer   replicated   1/1        dockersamples/visualizer:stable       *:8080->8080/tcp
mioomfkfo6ce   myapp_web          replicated   2/2        cesaraguilarmendez/comptador:latest   *:80->80/tcp
```

```
a221351ca@i17:~/m14-reptes/reptes_swarm/repte2$ docker service ps myapp_web
ID             NAME          IMAGE                                 NODE      DESIRED STATE   CURRENT STATE           ERROR     PORTS
dmufrttoef9s   myapp_web.1   cesaraguilarmendez/comptador:latest   i17       Running         Running 5 minutes ago             
esovglvsfub0   myapp_web.2   cesaraguilarmendez/comptador:latest   i17       Running         Running 5 minutes ago             
a221351ca@i17:~/m14-reptes/reptes_swarm/repte2$ docker stack rm myapp
Removing service myapp_redis
Removing service myapp_visualizer
Removing service myapp_web
Removing network myapp_mynet
```
