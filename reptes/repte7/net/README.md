#Repte7

## copiar els serveis interns de xinetd: echo, daytime, chargen.
Són archius copiats d'un container
```
a221351ca@i17:/var/tmp/m14-reptes/repte7$ docker cp prueba:/etc/xinetd.d/daytime .
Successfully copied 2.05kB to /var/tmp/m14-reptes/repte7/.

a221351ca@i17:/var/tmp/m14-reptes/repte7$ docker cp prueba:/etc/xinetd.d/chargen .
Successfully copied 2.56kB to /var/tmp/m14-reptes/repte7/.

a221351ca@i17:/var/tmp/m14-reptes/repte7$ docker cp prueba:/etc/xinetd.d/echo .
Successfully copied 2.05kB to /var/tmp/m14-reptes/repte7/.
```
modificar a tots tres archius disable a no.
Sense modificar:
```	disable		= yes ```

Modificat:
```	disable		= no ```



## Dockerfile:
```Dockerfile
# Servidor NET: serveis de xarxa echo,daytime,chrgen,ftp,tftp i http
FROM debian:latest
LABEL author="César Aguilar Méndez"
RUN apt-get update

#xinetd (echo,daytime,chargen)
RUN apt-get install -y telnet xinetd ftp tftp iproute2 iputils-ping nmap procps net-tools apt-utils apache2


RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh

#Echo: port 7, Daytime port: 13, Chargen port: 19, tfp port: 21 , tftp port: 69, http port: 80
EXPOSE 7 13 19 21 69 80
```

## startup.sh
```startup.sh
#! /bin/bash

#Copiar archius echo,chargen, daytime al  directory /etc/xinetd.d/

cp /opt/docker/chargen /etc/xinetd.d
cp /opt/docker/echo /etc/xinetd.d
cp /opt/docker/daytime /etc/xinetd.d

#Engegar xinetd
xinetd -dontfork

#Engegar apache en forground
apachectl -k start -X
```

## Comprovar
Engegar un altre container en la mateix net i fer la comanda: 
tlenet <ip> <port>

```
root@client:/# telnet 172.18.0.2 13
Trying 172.18.0.2...
Connected to 172.18.0.2.
Escape character is '^]'.
25 OCT 2023 10:50:15 UTC
Connection closed by foreign host.
root@client:/# telnet 172.18.0.2 7 
Trying 172.18.0.2...
Connected to 172.18.0.2.
Escape character is '^]'.
d
d
f
f
telnet> 
Connection closed.
```
