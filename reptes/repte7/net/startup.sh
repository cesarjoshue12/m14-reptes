#! /bin/bash

#Copiar archius echo,chargen, daytime al  directory /etc/xinetd.d/
cp /opt/docker/index.html /var/www/html/index.html
cp /opt/docker/style.css /var/www/html/style.css
cp /opt/docker/chargen /etc/xinetd.d
cp /opt/docker/echo /etc/xinetd.d
cp /opt/docker/daytime /etc/xinetd.d

systemctl start apache2 

#Engegar xinetd
xinetd -dontfork

#Engegar apache en forground
apachectl -k start -X

