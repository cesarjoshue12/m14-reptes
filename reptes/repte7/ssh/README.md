# REPTE7 SSH
## Dockerfile
```DockerfileFROM debian
LABEL author="César Aguilar Méndez"
LABEL subject="SSH server"
RUN apt-get update
RUN apt-get install -y openssh-client openssh-server procps vim
COPY useradd.sh /tmp/
RUN bash /tmp/useradd.sh
WORKDIR /tmp/
CMD /etc/init.d/ssh start -D
EXPOSE 22
```
## Useradd.sh
Creacio d'usuaris on la contrasenya serà el nom d'usuari
- ex user: unix1, passwd: unix1
```
#! /bin/bash

# Creació dels usuaris per verificat accés SSH
for user in unix1 unix2 unix3
do
	useradd -m $user
	echo $user:$user | chpasswd
done
```
## Ordres:
- docker run --rm --name ssh -h ssh.edt.org -p 22 -d cesaraguilarmendez/ssh
- docker exec -it ssh /bin/bash
```
root@ssh:/opt/docker# ssh unix1@localhost
The authenticity of host 'localhost (127.0.0.1)' can't be established.
ED25519 key fingerprint is SHA256:ei5ZnO/zQERMD3qyTs2t3aKdyjrX3KhOCCucVbifIUA.
This key is not known by any other names.
Are you sure you want to continue connecting (yes/no/[fingerprint])? Y
Please type 'yes', 'no' or the fingerprint: yes
Warning: Permanently added 'localhost' (ED25519) to the list of known hosts.
unix1@localhost's password: 
Linux ssh.edt.org 6.1.0-9-amd64 #1 SMP PREEMPT_DYNAMIC Debian 6.1.27-1 (2023-05-08) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
$
```
Fer un ip per despres connectarnos amb ssh: user@ip
```
$ ip a    
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
32: eth0@if33: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc noqueue state UP group default 
    link/ether 02:42:ac:11:00:02 brd ff:ff:ff:ff:ff:ff link-netnsid 0
    inet 172.17.0.2/16 brd 172.17.255.255 scope global eth0
       valid_lft forever preferred_lft forever
```

## ssh client
```
docker run --rm --name client -h client.edt.org -p 22 -it cesaraguilarmendez/ssh /bin/bash
root@client:/opt/docker# ssh unix1@172.17.0.2
The authenticity of host '172.17.0.2 (172.17.0.2)' can't be established.
ED25519 key fingerprint is SHA256:ei5ZnO/zQERMD3qyTs2t3aKdyjrX3KhOCCucVbifIUA.
This key is not known by any other names.
Are you sure you want to continue connecting (yes/no/[fingerprint])? yes
Warning: Permanently added '172.17.0.2' (ED25519) to the list of known hosts.
unix1@172.17.0.2's password: 
Linux ssh.edt.org 6.1.0-9-amd64 #1 SMP PREEMPT_DYNAMIC Debian 6.1.27-1 (2023-05-08) x86_64

The programs included with the Debian GNU/Linux system are free software;
the exact distribution terms for each program are described in the
individual files in /usr/share/doc/*/copyright.

Debian GNU/Linux comes with ABSOLUTELY NO WARRANTY, to the extent
permitted by applicable law.
Last login: Thu Nov  2 11:42:35 2023 from 127.0.0.1
$ 

```
 
