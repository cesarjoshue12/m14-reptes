
import time
import redis # base de dades ClauValor
from flask import Flask #eina per desenvolupar pagines web

# variable aplicació app
app = Flask(__name__)
# variable cache que contacta amb el redis
cache = redis.Redis(host='redis',port=6379)

def get_hit_count():
    # vegades que intentara
    retries = 5
    while True:
        try:
            return cache.incr('hits')
        except redis.exceptions.ConnectionError as exc:
            if retries == 0:
                raise exc
            retries -= 1
            time.sleep(0.5)


# Programa principal
# Ruta de l'aplicació arrel (index)
@app.route('/')
# funció hello que retorna les visites
def hello():
    count = get_hit_count()
    return 'Hello World! I have {} times ;).\n'.format(count)

@app.route('/patata')
# funció hello que retorna les visites
def myhola():
    count = get_hit_count()
    return 'Per papaya el lewandosky hola'

