#!/bin/bash
arg2=$2

if [ "$#" -eq 0 ]; then
	ordre="res"
else
	ordre="$1"
fi


case $ordre in 
	"initdbedt")
		#inicialitzar tot de nou  i fer el populate de edt.org
		# 1)Esborrar els directoris de configuració i de dadesi
		rm -rf /var/lib/ldap/*
		rm -rf /etc/ldap/slapd.d/*

		# 2)Generar el directori de configuració dinàmica slapd.d a partir del fitxer de configuració slapd.conf
		slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d

		# 3)Injectar a baix nivell les dades de la BD 'populate' de l'organització dc=edt,dc=org
		slapadd -F /etc/ldap/slapd.d/ -l /opt/docker/edt-org.ldif

		# 4)Assignar la propietat i grup del directori de ddaes i de configuració a l'usuari openldap
		chown -R openldap:openldap /etc/ldap/slapd.d /var/lib/ldap

		# 5)Engegar el servei slapd amb el paràmetre que fa debug per mantenir-lo engegat en foreground

		/usr/sbin/slapd -d0;;

	
	"slapd")
		echo "inicicialitzant ldap sense dades"
		rm -rf /var/lib/ldap/*
		rm -rf /etec/slapd.d/*
		slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
		chown -R openldap:openldap /etc/ldap/slapd.d /var/lib/ldap;;
	
	
	"start"|"edtorg"|"res")
		echo "inicialitzant ldap amb persistència"
		#Fer el docker run amb volums ldap-config ldap-data
		# rm -rf /var/lib/ldap/*
		# rm -rf /etc/ldap/slapd.d/*
		# slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
		# slapadd -F /etc/ldap/slapd.d/ -l /opt/docker/edt-org.ldif
		# chown -R openldap:openldap /etc/ldap/slapd.d /var/lib/ldap
		/usr/sbin/slapd -d0;;
	
	"slapcat")
		echo "fent slapcat"
		rm -rf /var/lib/ldap/*
		rm -rf /etc/ldap/slapd.d/*
		slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
		slapadd -F /etc/ldap/slapd.d/ -l /opt/docker/edt-org.ldif
		chown -R openldap:openldap /etc/ldap/slapd.d /var/lib/ldap

		if [ $arg2 -eq 0 -o $arg2 -eq 1 ]; then
			slapcat -n $arg2
		elif [ -z "$arg2" ]; then
			slapcat
		fi
		;;
	*)
		echo "Uso: $0"
		echo "Ordres: initdbedt, slapd, start, edtorg, slapcat nº"
		exit 1
		;;
esac
