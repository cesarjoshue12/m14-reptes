# Repte 5

I## Dockerfile ENTRYPOINT
```Dockerfile
# ldapserver 2023
FROM debian:latest
LABEL author="César Aguilar Méndez"
RUN apt-get update
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get install -y procps iproute2 tree nmap vim ldap-utils slapd less
RUN mkdir /opt/docker/
COPY * /opt/docker
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
ENTRYPOINT ["/bin/bash", "/opt/docker/startup.sh"]
EXPOSE 389
```
- ARG DEBIAN_FRONTED=noninteractive: Crea una avrieble d'entorn al instalar paquets ho fa demanera no interactiva

- ENTRYPOINT: Aquesta comanda el que fa es executar una shell amb les comandes
que introduïm dins del fitxer startup.sh, s'executara l'ordre indicada al docker run

## Explicació Startup.sh
```
1. Inicialitza tot de nou i fa populate de edt.org

rm -rf /etc/ldap/slapd.d/*
rm -rf /var/lib/ldap/*
slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
slapadd -F /etc/ldap/slapd.d/ -l /opt/docker/edt-org.ldif
chown -R openldap:openldap /etc/ldap/slapd.d /var/lib/ldap
/usr/sbin/slapd -d0;;


    - Eliminem directoris configuració i dades de ldap.
    - Creem directori dinàmic de configuració
    - Fem injecció de dades a baix nivell
    - Canviem propietari de root a openldap de la bd.
    - Encenem el dimoni en segon pla.

2. Ho inicialitza tot pero sense dades

    - Les mateixes comandes de abans però sense fer la injecció (slapadd).  

3. Engega server utilitzant persistència de dades i que perdurin les execucions ja existents.

    - Creem 2 volums per les dades de ldap i per la configuració.
        - docker volume create ldap-config al fer -v /etc/ldap/slapd.d és repositori de la config de ldap
        - docker volume create ldap-dades al fer -v /var/lib/ldap és repositori de les dades de ldap

    - Encenem container amb la següent ordre:
        - docker run --rm --name repte5 -h ldap.edt.org -v ldap-config:/etc/ldap/slapd.d -v ldap-dades:/var/lib/ldap cesaraguilarmendez/repte5

4. Fa slapcat de la base de dades seleccionada

    - Al startup.sh les ordres són les mateixes que en les altres opcions però dins d'un case, depenent del argument que ens passin
    (sigui buit, 0 o 1) ens fara un slapcat de la bd en qüestió.

```
## Encenem el container
```
docker run --rm --name repte5 -h repte5.edt.org cesaraguilarmendez/repte5 <opcions>
```
Opcions: 
-initdb
-slapd
-start/edtorg/res
-slapcat:0,1 o res
