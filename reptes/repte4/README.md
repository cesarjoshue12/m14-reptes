# Repte 3 Utilització de volums en un servidor Postgres

## Crear Dockerfile
```
# ldapserver 2023
FROM debian:latest
LABEL version="1.0"
LABEL author="@edt ASIX-M06"
RUN apt-get update
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get install -y procps iproute2 tree nmap vim ldap-utils slapd less
RUN mkdir /opt/docker/
COPY * /opt/docker
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
EXPOSE 389
```

## Crear startup.sh
´´´
#!/bin/bash

echo "Configurant el servidor ldap..."

# 1)Esborrar els directoris de configuració i de dadesi
rm -rf /var/lib/ldap/*
rm -rf /etc/ldap/slapd.d/*

# 2)Generar el directori de configuració dinàmica slapd.d a partir del fitxer de configuració slapd.conf
slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d

# 3)Injectar a baix nivell les dades de la BD 'populate' de l'organització dc=edt,dc=org
slapadd -F /etc/ldap/slapd.d/ -l /opt/docker/edt-org.ldif

# 4)Assignar la propietat i grup del directori de ddaes i de configuració a l'usuari openldap
chown -R openldap:openldap /etc/ldap/slapd.d /var/lib/ldap

# 5)Engegar el servei slapd amb el paràmetre que fa debug per mantenir-lo engegat en foreground

/usr/sbin/slapd -d0

´´´

## Crear archiu del contigut de la base de dades edt-org.ldif i de configuració slapd.conf
Els dos fitxers extrets de: 
```
https://github.com/edtasixm06/ldap22/tree/main/ldap22:base
```
## Fer el docker build
```
docker build -t cesaraguilarmendez/repte4 .

```

## Crear un volum per la configuració i dades de la base
```
docker volume create ldap-config
docker volume create ldap-data
```

## Engegar container amb persistencia de dades
Utilitzarem l'opció -v per asignar els volums creats
```
docker run --rm --name ldap.edt.org -h ldap.edt.org -p 389:389 -v ldap-config:/etc/ldap/slapd.d -v ldap-data:/var/lib/ldap -d cesaraguilarmendez/repte4
```

### Per comprovar he esborrat l'usuari pau i al tornar a engegar el container no es troba:
```
Primer llistem l'usuari pau:
a221351ca@i17:/var/tmp/m14-reptes/repte4$ ldapsearch -x -LLL -H ldap://172.17.0.2 -b 'dc=edt,dc=org' uid=pau dn
dn: cn=Pau Pou,ou=usuaris,dc=edt,dc=org

a221351ca@i17:/var/tmp/m14-reptes/repte4$ ldapdelete -vx -H ldap://172.17.0.2 -D cn=Manager,dc=edt,dc=org -w secret "cn=Pau Pou,ou=usuaris,dc=edt,dc=org" 
ldap_initialize( ldap://172.17.0.2:389/??base )
deleting entry "cn=Pau Pou,ou=usuaris,dc=edt,dc=org"

a221351ca@i17:/var/tmp/m14-reptes/repte4$ ldapsearch -x -LLL -H ldap://172.17.0.2 -b 'dc=edt,dc=org' uid=pau dn


Al tornar ha engegar el container comentem los dues primeres linies
```
