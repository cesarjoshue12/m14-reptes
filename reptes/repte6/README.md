# Repte 6

## Crear Dockerfile 
> Amb variables d'entorn LDAP_ROOTDN(dn del root) i LDAP_ROOTPW(passwd del root)
```dockerfile
FROM debian:latest
LABEL author="@César Aguilar Méndez"
RUN apt-get update
ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get install -y procps iproute2 tree nmap vim ldap-utils slapd less

RUN mkdir /opt/docker/
COPY * /opt/docker/
ENV LDAP_ROOTDN="Cesar"
ENV LDAP_ROOTPW="Cesar"
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
EXPOSE 389

```

## Crear startup.sh
Sed per editar les lines del fitxer slapd.conf on hi ha el root i la passwd i aquestes canviarles per la variable d'entorn del Dockerfile

```startup.sh
#!/bin/bash

echo "Configurant el servidor ldap..."

sed -i "s/Manager/$LDAP_ROOTDN/g" slapd.conf
sed -i "s/secret/$LDAP_ROOTPW/g" slapd.conf


1)Esborrar els directoris de configuració i de dadesi
rm -rf /var/lib/ldap/*
rm -rf /etc/ldap/slapd.d/*

2)Generar el directori de configuració dinàmica slapd.d a partir del fitxer de configuració slapd.conf
slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d

3)Injectar a baix nivell les dades de la BD 'populate' de l'organització dc=edt,dc=org
slapadd -F /etc/ldap/slapd.d/ -l /opt/docker/edt-org.ldif

4)Assignar la propietat i grup del directori de ddaes i de configuració a l'usuari openldap
chown -R openldap:openldap /etc/ldap/slapd.d /var/lib/ldap

5)Engegar el servei slapd amb el paràmetre que fa debug per mantenir-lo engegat en foreground

/usr/sbin/slapd -d0

```

## Fer el docker build
```
docker build -t cesaraguilarmendez/repte6 .

```

## Engegar container (sense detach no funciona)
```
docker run --rm --name repte6 -h ldap.edt.org cesaraguilarmendez/repte6
```

## Comprovar que s'ha canviat el root i la passwd:
Entrar al container i fer un cat del slapd.conf

```
$ docker exec -it repte6 /bin/bash
root@ldap:/opt/docker# cat slapd.conf 


# ----------------------------------------------------------------------
database mdb
suffix "dc=edt,dc=org"
rootdn "cn=Cesar,dc=edt,dc=org"
rootpw Cesar
directory /var/lib/ldap
index objectClass eq,pres
access to * by self write by * read
# ----------------------------------------------------------------------
database monitor
access to *
       by dn.exact="cn=Cesar,dc=edt,dc=org" read
       by * none

```
