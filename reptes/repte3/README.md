# Repte 3 Utilització de volums en un servidor Postgres

## Crear un volum
```
docker volume create postgres-data
```

## Crear container amb postgres i volum
```
docker run --name training -e POSTGRES_PASSWORD=secret -v postgres-data:/var/lib/postgresql/data -d postgres
d825468912ab25883e6af071db8a204d9dafeaf13adccd55660aca8b39469dd7
```

## Entrar dins del container i crear una base de dades i una taula
```
docker exec -it training psql -U postgres
psql (16.0 (Debian 16.0-1.pgdg120+1))
Type "help" for help.

postgres=# create database bot
postgres-# ;
CREATE DATABASE
postgres=# \c bot
You are now connected to database "bot" as user "postgres".
bot=# CREATE TABLE cesar();
CREATE TABLE
bot=# \d
     	List of relations
 Schema | Name  | Type  |  Owner   
--------+-------+-------+----------
 public | cesar | table | postgres
(1 row)
```
### Per comprovar he sortit del container i he tornat ha entrar i les dades continuen:
```
bot=# exit
a221351ca@i17:/var/tmp/m14-reptes/repte3$ docker exec -it training psql -U postgres
psql (16.0 (Debian 16.0-1.pgdg120+1))
Type "help" for help.
postgres=# \c bot
You are now connected to database "bot" as user "postgres".
bot=# \d
     	List of relations
 Schema | Name  | Type  |  Owner   
--------+-------+-------+----------
 public | cesar | table | postgres
(1 row)
```
