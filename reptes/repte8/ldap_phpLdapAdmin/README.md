# PHPLDAPADMINER

## Compose.yml
```compose.yml
version: '2'
services:
  ldap:
    image: cesaraguilarmendez/ldap23:latest
    container_name: ldap.edt.org
    hostname: ldap.edt.org
    ports:
      - 389:389
    networks:
      - mynet
  phpldapadmin:
    image: cesaraguilarmendez/phpldapadmin
    container_name: phpldapadmin.edt.org
    hostname: phpldapadmin.edt.org
    ports:
      - 80:80
    networks:
      - mynet
networks:
  mynet:
```

## Ordres:
- Engegar: docker compose up -d
- Parar: docker compose down

## Comprovar
Al buscador posar: 
```localhost/phpldapadmin```

Login:
```
Login DN: cn=Manager,dc=edt,dc=org

password: passwd
```

