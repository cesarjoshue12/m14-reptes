Crear fitxer yml;

```
# Use postgres/passwd credentials
version: '2'
services:
  db:
    image: cesaraguilarmendez/postgres
    container_name: postgres.edt.org
    hostname: postgres.edt.org
    restart: always
    environment:
      POSTGRES_PASSWORD: passwd
      POSTGRES_DB: training
    networks:
      - mynet
  adminer:
    image: adminer
    container_name: adminer
    hostname: adminer.edt.org
    restart: always
    ports:
      - 8080:8080
    networks:
      - mynet
networks:
  mynet:

```

Per fer connexio:
```localhost:8080```

el port 8080 perquè és el que he posat al fitxer yml.
Conectarnos:
- System: PostgreSQL
- Server: db
- Username: postgres
- Password: passwd
- Database: training


